﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class DeleteUserViewModel
    {
        public string YesOrNo { get; set; }
    }
}
