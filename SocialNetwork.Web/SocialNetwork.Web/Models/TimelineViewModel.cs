﻿using SocialNetwork.Services.DTOs;

namespace SocialNetwork.Web.Models
{
    public class TimelineViewModel
    {
        public UserDTO UserDTO { get; set; }
        public CommentViewModel CommentViewModel { get; set; }
    }
}
