﻿using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using System;
using System.Threading.Tasks;

namespace SocialNetwork.Web.API_Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserAPIController : ControllerBase
    {
        private readonly IUserService _service;
        public UserAPIController(IUserService service)
        {
            this._service = service;
        }
        //APIOK
        [HttpPost("")]
        public async Task<IActionResult> CreateUser([FromBody] UserDTO userDTO)
        {
            var user = await _service.CreateUserAsync(userDTO);
            return new JsonResult(user);
        }
        //APIOK
        [HttpDelete("id={id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _service.DeleteUserAsync(id);
            return new JsonResult(user);
        }
        //APIOK
        [HttpGet("")]
        public async Task<IActionResult> GetAllUsers()
        {
            var users = await _service.GetAllUsersAsync();
            return Ok(users);
        }
        //APIOK
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserById(int id)
        {
            try
            {
                var user = await _service.GetUserAsync(id);
                return Ok(user);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }
        //APIOK
        [HttpGet("userByName")]
        public async Task<IActionResult> GetUserByName([FromQuery] string email)
        {
            try
            {
                var user = await _service.GetUserAsync(email);
                return Ok(user);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }
        //APIOK
        [HttpPut("")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody] UserDTO userDTO)
        {
            var user = await _service.UpdateUserInformationAfterLoginAsync(id, userDTO);
            return new JsonResult(user);
        }
    }
}