﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Contracts;

namespace SocialNetwork.Web.API_Controllers
{
    [Route("api/country")]
    [ApiController]
    public class CountryAPIController : ControllerBase
    {
        private readonly ICountryService _service;
        public CountryAPIController(ICountryService service)
        {
            this._service = service;
        }
        //APIOK
        [HttpPost("")]
        public async Task<IActionResult> CreateCountry([FromBody] CountryDTO countryDTO)
        {
            var country = await _service.CreateCountryAsync(countryDTO);
            return new JsonResult(country);
        }
        //APIOK
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCountry(int id)
        {
            var country = await _service.DeleteCountryAsync(id);
            return new JsonResult(country);
        }
        //APIOK
        [HttpGet("")]
        public async Task<IActionResult> GetAllCountries()
        {
            var country = await _service.GetAllCountriesAsync();
            return Ok(country);
        }
        //APIOK
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCountry(int id)
        {
            try
            {
                var country = await _service.GetCountryAsync(id);
                return Ok(country);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }
        //APIOK
        [HttpPut("")]
        public async Task<IActionResult> UpdateCountry([FromQuery] int id, [FromQuery] string newName)
        {
            var country = await _service.UpdateCountryAsync(id, newName);
            return new JsonResult(country);
        }
    }
}
