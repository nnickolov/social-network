﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Contracts;

namespace SocialNetwork.Web.API_Controllers
{
    [Route("api/comment")]
    [ApiController]
    public class CommentAPIController : ControllerBase
    {
        private readonly ICommentService _service;
        public CommentAPIController(ICommentService service)
        {
            this._service = service;
        }
        //APIOK
        [HttpPost("")]
        public async Task<IActionResult> CreateComment(int id, int postId, [FromBody] CommentDTO commentDTO)
        {
            var comment = await _service.CreateCommentAsync(id, postId, commentDTO);
            return new JsonResult(comment);
        }
        //APIOK
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComment(int id, int userId)
        {
            var comment = await _service.DeleteCommentAsync(id, userId);
            return new JsonResult(comment);
        }
        //APIOK
        [HttpGet("")]
        public async Task<IActionResult> GetAllComments()
        {
            var comment = await _service.GetAllCommentsAsync();
            return Ok(comment);
        }
        //APIOK
        [HttpGet("{id}")]
        public async Task<IActionResult> GetComment(int id)
        {
            try
            {
                var comment = await _service.GetCommentAsync(id);
                return Ok(comment);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }
        //APIOK
        [HttpPut("")]
        public async Task<IActionResult> UpdateComment([FromQuery] int id,[FromQuery] string newContent)
        {
            var comment = await _service.UpdateCommentAsync(id, newContent);
            return new JsonResult(comment);
        }
        //APIOK
        [HttpPut("like/id={id}")]
        public async Task<IActionResult> LikeComment(int id)
        {
            var comment = await _service.LikeCommentAsync(id);
            return new JsonResult(comment);
        }
        //APIOK
        [HttpPut("flag/id={id}")]
        public async Task<IActionResult> FlagComment(int id)
        {
            var comment = await _service.FlagCommentAsync(id);
            return new JsonResult(comment);
        }
    }
}
