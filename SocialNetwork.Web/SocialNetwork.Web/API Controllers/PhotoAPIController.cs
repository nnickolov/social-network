﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;

namespace SocialNetwork.Web.API_Controllers
{
    [Route("api/photo")]
    [ApiController]
    public class PhotoAPIController : ControllerBase
    {
        private readonly IPhotoService _service;
        public PhotoAPIController(IPhotoService service)
        {
            this._service = service;
        }
        //APIOK
        [HttpPost("")]
        public async Task<IActionResult> UploadPhoto(int id, [FromBody] PhotoDTO photoDTO)
        {
            var photo = await _service.UploadPhotoAsync(0, id, photoDTO);
            return new JsonResult(photo);
        }
        //APIOK
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhoto(int id)
        {
            var photo = await _service.DeletePhotoAsync(id);
            return new JsonResult(photo);
        }
    }
}
