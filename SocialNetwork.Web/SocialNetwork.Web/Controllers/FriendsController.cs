﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;

namespace SocialNetwork.Web.Controllers
{
    public class FriendsController : Controller
    {

        private readonly ILogger<HomeController> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ICommentService _commentService;
        private readonly IContactService _contactService;
        private readonly IPhotoService _photoService;
        private readonly IPostService _postService;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;


        public FriendsController(IPostService postService, ICommentService commentService, ILogger<HomeController> logger, IMapper mapper, IContactService contactService, IPhotoService photoService, IUserService userService, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this._logger = logger;
            this._mapper = mapper;
            this._contactService = contactService;
            this._photoService = photoService;
            this._userService = userService;
            this._signInManager = signInManager;
            this._userManager = userManager;
            this._postService = postService;
            this._commentService = commentService;
        }

        // GET: Friends
        public async Task<IActionResult> Index()
        {
            var curr = _userManager.GetUserId(User);
            var users = await _userService.GetUserAsync(int.Parse(curr));

            return View(users);
        }

        // GET: Friends/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Friends/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Friends/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Friends/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Friends/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Friends/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Friends/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
