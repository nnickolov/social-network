﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class CountriesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICountryService _countryService;
        private readonly SignInManager<User> _signInManager;

        public CountriesController(SignInManager<User> signInManager, ICountryService countryService, IMapper mapper)
        {
            this._mapper = mapper;
            this._signInManager = signInManager;
            this._countryService = countryService;
        }

        // GET: Countries
        public async Task<IActionResult> Index()
        {
            var countries = await _countryService.GetAllCountriesAsync();
            return View(countries);
        }

        // GET: Countries/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var country = await _countryService.GetCountryAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        // GET: Countries/Create
        public IActionResult Create()
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            return View();
        }

        // POST: Countries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CountryViewModel countryViewModel)
        {
            if (ModelState.IsValid)
            {
                var countryDTO = _mapper.Map<CountryDTO>(countryViewModel);

                await _countryService.CreateCountryAsync(countryDTO);

                return RedirectToAction(nameof(Index));
            }
            // ViewData["PostId"] = new SelectList(await _postService.GetAllPostsAsync(), "Id", "Id", commentDTO.PostId);
            return View();
        }

        // GET: Countries/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            var country = await _countryService.GetCountryAsync(id);

            if (country == null)
            {
                return NotFound();
            }
            return View(country);
        }

        // POST: Countries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CountryViewModel countryViewModel)
        {
            var countryDTO = _mapper.Map<CountryDTO>(countryViewModel);

            if (ModelState.IsValid)
            {
                await _countryService.UpdateCountryAsync(id, countryDTO.Name);

                return RedirectToAction(nameof(Index));
            }
            return View(countryDTO);
        }

        // GET: Countries/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            var country = await _countryService.GetCountryAsync(id);

            return View(country);
        }

        // POST: Countries/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _countryService.DeleteCountryAsync(id);

            return RedirectToAction(nameof(Index));
        }
    }
}
