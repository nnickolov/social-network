﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ICountryService _countryService;
        private readonly SignInManager<User> _signInManager;

        public UsersController(SignInManager<User> signInManager, IUserService userService, IMapper mapper, ICountryService countryService)
        {
            this._mapper = mapper;
            this._countryService = countryService;
            this._userService = userService;
            this._signInManager = signInManager;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var users = _userService.GetAllUsersAsync();
            return View(await users);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var user = await _userService.GetUserAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        } 
        public async Task<IActionResult> Details(string email)
        {
            var user = await _userService.GetUserAsync(email);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegisterViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                var userDTO = _mapper.Map<UserDTO>(userViewModel);

                await _userService.CreateUserAsync(userDTO);

                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Users/NewsFeed
       

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            var user = await _userService.GetUserAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            ViewData["CountryId"] = new SelectList(await _countryService.GetAllCountriesAsync(), "Id", "Id", user.CountryId);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, RegisterViewModel userViewModel)
        {
            var userDTO = _mapper.Map<UserDTO>(userViewModel);

            if (ModelState.IsValid)
            {
                await _userService.UpdateUserInformationAfterLoginAsync(id, userDTO);

                return RedirectToAction(nameof(Index));
            }
            return View(userDTO);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            var user = await _userService.GetUserAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _userService.DeleteUserAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
