﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class MessagesController : Controller
    {
        private readonly IMessageService _messageService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public MessagesController(IMapper mapper, IUserService userService, IMessageService messageService, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this._mapper = mapper;
            this._userService = userService;
            this._messageService = messageService;
            this._userManager = userManager;
            this._signInManager = signInManager;
        }

        // GET: Messages
        public async Task<IActionResult> Index(int receiverId)
        {
            var user = _userManager.GetUserId(User);

            if (receiverId == 0)
            {
                var currentUser = await _userService.GetUserAsync(int.Parse(user));
                return View(currentUser);

            }
            else
            {
                var currentUser = await _userService.GetUserAsync(int.Parse(user));
                currentUser.CurrentLoggedUserId = receiverId;
                return View(currentUser);
            }




        }

        // GET: Messages/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var message = await _messageService.GetMessageAsync(id);

            if (message == null)
            {
                return NotFound();
            }

            return View(message);
        }


        // POST: Messages/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int userId, int receiverId, UserDTO userDTO)
        {
            if (ModelState.IsValid)
            {
                var messageDTO = _mapper.Map<MessageDTO>(userDTO);

                messageDTO.SenderId = userId;
                messageDTO.ReceiverId = receiverId;
                messageDTO.Text = userDTO.NewCommentContent;

                await _messageService.CreateMessageAsync(messageDTO);

                return RedirectToAction("Index", new { receiverId = receiverId });

            }
            //TODO
            return NotFound();
        }
    }
}
