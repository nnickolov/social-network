﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;

namespace SocialNetwork.Web.Controllers
{
    public class UserFriendsController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IUserFriendService _userFriendService;
        private readonly ICommentService _commentService;
        private readonly IContactService _contactService;
        private readonly IPhotoService _photoService;
        private readonly IPostService _postService;
        private readonly IWebHostEnvironment _environment;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;


        public UserFriendsController(IUserFriendService userFriendService, IPostService postService, ICommentService commentService, ILogger<HomeController> logger, IWebHostEnvironment environment, IMapper mapper, IContactService contactService, IPhotoService photoService, IUserService userService, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this._logger = logger;
            this._environment = environment;
            this._mapper = mapper;
            this._contactService = contactService;
            this._photoService = photoService;
            this._userService = userService;
            this._signInManager = signInManager;
            this._userManager = userManager;
            this._postService = postService;
            this._commentService = commentService;
            this._userFriendService = userFriendService;
        }

        public async Task<IActionResult> FriendRequests()
        {
            string curentUser = _userManager.GetUserId(User);

            var user = await _userService.GetUserAsync(int.Parse(curentUser));

            return View(user);
        }

        public async Task<IActionResult> Accept(int newsfeed, int timelineOfFriendId, int toAddId)
        {
            if (timelineOfFriendId == 0)
            {
                await _userFriendService.AcceptFriendRequest(toAddId);
                return RedirectToAction("FriendRequests", "UserFriends");
            }
            if (newsfeed != 0)
            {
                await _userFriendService.AcceptFriendRequest(toAddId);
                return RedirectToAction("Newsfeed", "Index");
            }
            else
            {
                await _userFriendService.AcceptFriendRequest(toAddId);
                return RedirectToAction("TimelineOfFriend", "Home", new { id = timelineOfFriendId});
            }

        }

        public async Task<IActionResult> Decline(int newsfeed, int timelineOfFriendId, int toAddId)
        {
            if (timelineOfFriendId == 0)
            {
                await _userFriendService.DeclineFriendRequest(toAddId);
                return RedirectToAction("FriendRequests", "UserFriends");
            }
            if (newsfeed != 0)
            {
                await _userFriendService.DeclineFriendRequest(toAddId);
                return RedirectToAction("Newsfeed", "Index");
            }
            else
            {
                await _userFriendService.DeclineFriendRequest(toAddId);
                return RedirectToAction("TimelineOfFriend", "Home", new { id = timelineOfFriendId });
            }
        }

        //TODO make all the actions return the needed view. Make new return options if you call the unfriend of the timeline of friend or friends view.
        public async Task<IActionResult> Unfriend(int newsfeed, int timelineOfFriendId, int Id)
        {
            if (timelineOfFriendId == 0)
            {
                await _userFriendService.RemoveFriend(Id);
                return RedirectToAction("FriendRequests", "UserFriends");
            }
            if (newsfeed != 0)
            {
                await _userFriendService.RemoveFriend(Id);
                return RedirectToAction("Newsfeed", "Index");
            }
            else
            {
                await _userFriendService.RemoveFriend(Id);
                return RedirectToAction("TimelineOfFriend", "Home", new { id = timelineOfFriendId });
            }
        }

    }
}
