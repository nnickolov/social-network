﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class CommentsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;
        private readonly IUserService _userService;
        private readonly ICommentService _commentService;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public CommentsController(UserManager<User> userManager, IUserService userService, SignInManager<User> signInManager, ICommentService commentService, IMapper mapper, IPostService postService)
        {
            this._mapper = mapper;
            this._postService = postService;
            this._userManager = userManager;
            this._userService = userService;
            this._signInManager = signInManager;
            this._commentService = commentService;
        }
        //DO NOT TOUCH
        public async Task<IActionResult> LikesUp(int newsfeedId, int commentId, int userId)
        {
            if (ModelState.IsValid)
            {
                await _commentService.LikeCommentAsync(commentId);

                if (newsfeedId == 1)
                {
                    return RedirectToAction("Index", "Newsfeed");
                }

                else if (newsfeedId == 2)
                {
                    return RedirectToAction("TimelineOfFriend", "Home", new { id = userId });
                }

                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        //DO NOT TOUCH
        public async Task<IActionResult> FlagsUp(int newsfeedId, int commentId, int userId)
        {
            if (ModelState.IsValid)
            {
                await _commentService.FlagCommentAsync(commentId);

                if (newsfeedId == 1)
                {
                    return RedirectToAction("Index", "Newsfeed");
                }
                else if (newsfeedId == 2)
                {
                    return RedirectToAction("TimelineOfFriend", "Home", new { id = userId });
                }
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        //Delete DO NOT TOUCH!!!!!!!
        public async Task<IActionResult> DeleteComment(int newsfeedId, int commentId, int userId)
        {
            var comment = await _commentService.DeleteCommentAsync(commentId, userId);

            if (newsfeedId == 1)
            {
                return RedirectToAction("Index", "Newsfeed");
            }
            else if (newsfeedId == 2)
            {
                return RedirectToAction("TimelineOfFriend", "Home", new { id = userId });
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
