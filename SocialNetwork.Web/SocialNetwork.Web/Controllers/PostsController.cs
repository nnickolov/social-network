﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class PostsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;
        private readonly IUserService _userService;
        private readonly SignInManager<User> _signInManager;

        public PostsController(SignInManager<User> signInManager, IUserService userService, IMapper mapper, IPostService postService)
        {
            this._mapper = mapper;
            this._postService = postService;
            this._signInManager = signInManager;
            this._userService = userService;
        }

        // GET: Posts
        public async Task<IActionResult> Index()
        {
            var allPost = _postService.GetAllPostsAsync();
            return View(await allPost);
        }

        // GET: Posts/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            var post = await _postService.GetPostAsync(id);

            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Posts/Create
        [HttpPost]
        public async Task<IActionResult> CreatePost(int loggedUser, int newsfeedId, int id, UserDTO userDTO)
        {
            var postCommentDTO = _mapper.Map<PostDTO>(userDTO);

            postCommentDTO.Content = userDTO.NewCommentContent;
            postCommentDTO.UserId = id;

            await _postService.CreatePostAsync(loggedUser,id, postCommentDTO);
            if (loggedUser != 0)
            {
                return RedirectToAction("TimelineOfFriend", "Home", new { id = id});
            }
            if (newsfeedId == 1)
            {
                return RedirectToAction("Index", "Newsfeed");
            }

            return RedirectToAction("Index", "Home");
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.


        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("https://localhost:5001/Identity/Account/Login%22");
            }

            var post = await _postService.GetPostAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PostViewModel postViewModel)
        {
            var postDTO = _mapper.Map<PostDTO>(postViewModel);

            if (ModelState.IsValid)
            {
                await _postService.UpdatePostAsync(id, postDTO);

                return RedirectToAction(nameof(Index));
            }
            return View(postDTO);
        }

        //Delete
        public async Task<IActionResult> DeletePost(int newsfeedId, int postId, int userId)
        {
            await _postService.DeletePostAsync(postId, userId);

            if (newsfeedId == 1)
            {
                return RedirectToAction("Index", "Newsfeed");
            }
            else if (newsfeedId == 2)
            {
                return RedirectToAction("TimelineOfFriend", "Home", new { id = userId });
            }
            return RedirectToAction("Index", "Home");
        }
        //Like
        public async Task<IActionResult> LikesUp(int newsfeedId, int postId, int userId)
        {
            if (ModelState.IsValid)
            {
                await _postService.LikePostAsync(postId);

                if (newsfeedId == 1)
                {
                    return RedirectToAction("Index", "Newsfeed");
                }

                else if (newsfeedId == 2)
                {
                    return RedirectToAction("TimelineOfFriend", "Home", new { id = userId });
                }

                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        //Flag
        public async Task<IActionResult> FlagsUp(int newsfeedId, int postId, int userId)
        {
            if (ModelState.IsValid)
            {
                await _postService.FlagPostAsync(postId);

                if (newsfeedId == 1)
                {
                    return RedirectToAction("Index", "Newsfeed");
                }
                
                else if (newsfeedId == 2)
                {
                    return RedirectToAction("TimelineOfFriend", "Home", new { id = userId });
                }
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}
