﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class NewsfeedController : Controller
    {
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;

        public NewsfeedController(IUserService userService, UserManager<User> userManager)
        { 
            this._userService = userService;
            this._userManager = userManager;
        }

        // GET: NewsfeedController
        public async Task<IActionResult> Index()
        {
            var user = _userManager.GetUserId(User);

            var currentUser = await _userService.GetUserAsync(int.Parse(user));

            currentUser.LoggedUserProfilePicture = currentUser.ProfilePhoto;
            return View(currentUser);
        }
    }
}
