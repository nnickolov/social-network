﻿using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IUserFriendService _userFriendService;
        private readonly ICommentService _commentService;
        private readonly IContactService _contactService;
        private readonly IPhotoService _photoService;
        private readonly IPostService _postService;
        private readonly IWebHostEnvironment _environment;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;


        public HomeController(IUserFriendService userFriendService, IPostService postService, ICommentService commentService, ILogger<HomeController> logger, IWebHostEnvironment environment, IMapper mapper, IContactService contactService, IPhotoService photoService, IUserService userService, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this._logger = logger;
            this._environment = environment;
            this._mapper = mapper;
            this._contactService = contactService;
            this._photoService = photoService;
            this._userService = userService;
            this._signInManager = signInManager;
            this._userManager = userManager;
            this._postService = postService;
            this._commentService = commentService;
            this._userFriendService = userFriendService;
        }

        public IActionResult Index()
        {

            if (_signInManager.IsSignedIn(User))
            {
                return RedirectToAction("Timeline");
            }
            else
            {
                Response.Redirect("Identity/Account/Register");
            }
            return View();
        }

        //GET
        public async Task<IActionResult> Friends(int id)
        {
            string curr = _userManager.GetUserId(User);
            if (id == 0)
            {
                var user = await _userService.GetUserAsync(int.Parse(curr));
                user.CurrentLoggedUserId = int.Parse(curr);

                return View(user);
            }
            else
            {
                var user = await _userService.GetUserAsync(id);
                user.CurrentLoggedUserId = int.Parse(curr);

                return View(user);
            }

        }
        //GET
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index");
        }

        //GET
        public IActionResult UpdateUserInformation()
        {
            return View();
        }
        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateUserInformation(UpdateUserInformationViewModel updateUserInformationViewModel)
        {
            if (ModelState.IsValid)
            {
                var userDTO = _mapper.Map<UserDTO>(updateUserInformationViewModel);

                string curentUser = _userManager.GetUserId(User);

                var random = new Random();
                var photoName = random.Next();


                var extension = updateUserInformationViewModel.Photo.FileName.Split(".").Last();
                var fileName = $"{photoName}.{extension}";
                var destPath = Path.Combine(_environment.WebRootPath, "images", fileName);

                using (var fileStream = new FileStream(destPath, FileMode.Create))
                {
                    updateUserInformationViewModel.Photo.CopyTo(fileStream);
                }

                var memoryStream = new MemoryStream();
                updateUserInformationViewModel.Photo.OpenReadStream().CopyTo(memoryStream);
                byte[] photoAsBytes = memoryStream.ToArray();

                var photoDTO = _mapper.Map<PhotoDTO>(updateUserInformationViewModel);
                photoDTO.PhotoAsNumber = photoName;
                photoDTO.Type = fileName;

                photoDTO.PhotoAsBytes = photoAsBytes;
                photoDTO.PhotoDescription = updateUserInformationViewModel.PhotoDescription;

                userDTO.ProfilePhoto = fileName;

                var randomCover = new Random();
                var photoNameCover = random.Next();

                var extensionCover = updateUserInformationViewModel.CoverPhoto.FileName.Split(".").Last();
                var fileNameCover = $"{photoNameCover}.{extension}";
                var destPathCover = Path.Combine(_environment.WebRootPath, "images", fileNameCover);

                using (var fileStreamCover = new FileStream(destPathCover, FileMode.Create))
                {
                    updateUserInformationViewModel.CoverPhoto.CopyTo(fileStreamCover);
                }

                var memoryStreamCover = new MemoryStream();
                updateUserInformationViewModel.CoverPhoto.OpenReadStream().CopyTo(memoryStreamCover);
                byte[] photoAsBytesCover = memoryStreamCover.ToArray();

                var photoDTOCover = _mapper.Map<PhotoDTO>(updateUserInformationViewModel);
                photoDTOCover.PhotoAsNumber = photoNameCover;
                photoDTOCover.Type = fileNameCover;

                photoDTOCover.PhotoAsBytes = photoAsBytesCover;

                userDTO.CoverPhoto = fileNameCover;

                userDTO.Visibility = updateUserInformationViewModel.Visibility;

                await _photoService.UploadPhotoAsync(0, int.Parse(_userManager.GetUserId(User)), photoDTO);

                await _photoService.UploadCoverPhotoAsync(int.Parse(_userManager.GetUserId(User)), photoDTOCover);

                await _userService.UpdateUserInformationAfterLoginAsync(int.Parse(_userManager.GetUserId(User)), userDTO);

                return RedirectToAction(nameof(Timeline));
            }
            return View();
        }
        // GET: Photos/Create
        public IActionResult CreatePhotoPost()
        {
            return View();
        }

        // POST: Photos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePhotoPost(int loggedUser, int newsfeedId, int id, PhotoPostViewModel photoPostViewModel)
        {
            if (ModelState.IsValid)
            {
                var userDTO = _mapper.Map<UserDTO>(photoPostViewModel);

                string curentUser = _userManager.GetUserId(User);

                var random = new Random();
                var photoName = random.Next();

                var extension = photoPostViewModel.Photo.FileName.Split(".").Last();
                var fileName = $"{ photoName}.{extension}";
                var destPath = Path.Combine(_environment.WebRootPath, "images", fileName);

                using (var fileStream = new FileStream(destPath, FileMode.Create))
                {
                    photoPostViewModel.Photo.CopyTo(fileStream);
                }

                var memoryStream = new MemoryStream();
                photoPostViewModel.Photo.OpenReadStream().CopyTo(memoryStream);
                byte[] photoAsBytes = memoryStream.ToArray();

                var photoDTO = _mapper.Map<PhotoDTO>(photoPostViewModel);

                photoDTO.Type = fileName;
                photoDTO.PhotoAsNumber = photoName;
                photoDTO.PhotoAsBytes = photoAsBytes;
                photoDTO.PhotoDescription = photoPostViewModel.PhotoDescription;

                await _photoService.UploadPhotoAsync(loggedUser, id, photoDTO);

                if (newsfeedId == 1)
                {
                    return RedirectToAction("Index", "Newsfeed");
                }
                else if (newsfeedId == 2)
                {
                    return RedirectToAction("TimelineOfFriend", "Home", new { id = id });
                }
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public async Task<IActionResult> Timeline()
        {
            string curentUser = _userManager.GetUserId(User);

            var user = await _userService.GetUserAsync(int.Parse(curentUser));

            user.LoggedUserProfilePicture = user.ProfilePhoto;
            return View(user);
        }
        public IActionResult SendFriendRequest(int messagesId, int newsfeed, int adresseeId)
        {
            string curentUser = _userManager.GetUserId(User);

            if (newsfeed == 1)
            {
                _userFriendService.SendFriendRequest(int.Parse(curentUser), adresseeId);
                return RedirectToAction("Index", "Newsfeed");
            }
            if (messagesId == 1)
            {
                _userFriendService.SendFriendRequest(int.Parse(curentUser), adresseeId);
                return RedirectToAction("Index", "Messages");
            }
            else
            {
                _userFriendService.SendFriendRequest(int.Parse(curentUser), adresseeId);

                return RedirectToAction("TimeLineOfFriend", new { id = adresseeId });
            }

        }
        public async Task<IActionResult> TimelineOfFriend(int id)
        {
            var user = await _userService.GetUserAsync(id);

            string curentUser = _userManager.GetUserId(User);

            var currentLogedUser = await _userService.GetUserAsync(int.Parse(curentUser));

            user.LoggedUserProfilePicture = currentLogedUser.ProfilePhoto;
            return View(user);
        }
        public IActionResult Contact()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Contact(ContactViewModel contactViewModel)
        {
            var userDTO = _mapper.Map<UserDTO>(contactViewModel);

            userDTO.FirstName = contactViewModel.FirstAndLastName;
            userDTO.Email = contactViewModel.Email;
            userDTO.NewCommentContent = contactViewModel.MessageContent;

            await _contactService.SendMessageToAdmin(userDTO);

            return RedirectToAction(nameof(SuccessfullySendMessageToAdmin));
        }
        public IActionResult SuccessfullySendMessageToAdmin()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateComment(int userId, int id, int timelineId, int postId, UserDTO userDTO)
        {
            if (ModelState.IsValid)
            {
                var commentDTO = _mapper.Map<CommentDTO>(userDTO);

                commentDTO.PostId = postId;
                commentDTO.Content = userDTO.NewCommentContent;
                commentDTO.UserId = userId;

                await _commentService.CreateCommentAsync(userId, postId, commentDTO);

                if (timelineId == 1)
                {
                    return RedirectToAction("Timeline", "Home");
                }
                else if (timelineId == 0)
                {
                    return RedirectToAction("Index", "Newsfeed");
                }
                else
                {
                    return RedirectToAction("TimelineOfFriend", "Home", new { id = timelineId });
                }
            }
            return View();
        }
        //Get
        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
