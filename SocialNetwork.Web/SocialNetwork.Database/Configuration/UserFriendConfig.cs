﻿using SocialNetwork.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SocialNetwork.Database.Configuration
{
    public class UserFriendConfig : IEntityTypeConfiguration<UserFriend>
    {
        public void Configure(EntityTypeBuilder<UserFriend> builder)
        {
            builder.HasOne(u => u.Requester)
                .WithMany(r => r.RequestersUserFriends)
                .HasForeignKey(a => a.RequesterId)
                .IsRequired();

            builder.HasOne(p => p.Adressee)
              .WithMany(u => u.AdresseesUserFriends)
              .HasForeignKey(p => p.AdresseeId)
              .IsRequired();
        }
    }
}