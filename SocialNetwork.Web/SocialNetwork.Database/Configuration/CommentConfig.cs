﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetwork.Models;


namespace SocialNetwork.Database.Configuration
{
    public class CommentConfig : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasOne(u => u.Post)
                .WithMany(c => c.Comments)
                .HasForeignKey(u => u.PostId);
            
            builder.HasOne(u => u.Photo)
                .WithMany(c => c.Comments)
                .HasForeignKey(u => u.PhotoId);
            
            builder.HasOne(u => u.User)
                .WithMany(c => c.Comments)
                .HasForeignKey(u => u.UserId);
        }
    }
}
