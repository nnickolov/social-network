﻿using SocialNetwork.Models;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SocialNetwork.Services.Mappers
{
    public static class HistoryDTOMapper
    {
        public static HistoryLogDTO GetDTO(this HistoryLog item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new HistoryLogDTO
            {
                Id = item.Id,
                Content= item.Content,
                UserId = item.UserId
            };
        }

        public static HistoryLog GetHistoryLog(this HistoryLogDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new HistoryLog
            {
                Id = item.Id,
                Content = item.Content,
                UserId = item.UserId
            };
        }
        public static ICollection<HistoryLogDTO> GetDTO(this ICollection<HistoryLog> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
