﻿using SocialNetwork.Models;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SocialNetwork.Services.Mappers
{
    public static class PhotoDTOMapper
    {
        public static PhotoDTO GetDTO(this Photo item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new PhotoDTO
            {
                Id = item.Id,
                UserId = item.UserId,
                PhotoAsNumber = item.PhotoAsNumber,
                Type = item.Type,
                User = item.User,
                Post = item.Post,
                PostId = item.PostId,
                PhotoAsBytes = item.PhotoAsBytes,
            };
        }

        public static Photo GetPhoto(this PhotoDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Photo
            {
                Id = item.Id,
                UserId = item.UserId,
                PhotoAsNumber = item.PhotoAsNumber,
                Type = item.Type,
                User = item.User,
                Post = item.Post,
                PostId = item.PostId,
                PhotoAsBytes = item.PhotoAsBytes,
            };
        }
        public static ICollection<PhotoDTO> GetDTO(this ICollection<Photo> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
