﻿using SocialNetwork.Models;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SocialNetwork.Services.Mappers
{
    public static class MessageDTOMapper
    {
        public static MessageDTO GetDTO(this Message item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new MessageDTO
            {
                Id = item.Id,
                SendOn = item.SendOn,
                Text = item.Text,
                IsDeleted = item.IsDeleted,
                Sender = item.Sender,
                SenderId = item.SenderId,
                Receiver = item.Receiver,
                ReceiverId = item.ReceiverId,
                isSeen = item.isSeen,
                UserName = item.UserName
            };
        }

        public static Message GetMessage(this MessageDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Message
            {
                Id = item.Id,
                SendOn = item.SendOn,
                Text = item.Text,
                IsDeleted = item.IsDeleted,
                Sender = item.Sender,
                SenderId = item.SenderId,
                Receiver = item.Receiver,
                ReceiverId = item.ReceiverId,
                isSeen = item.isSeen,
                UserName = item.UserName
            };
        }
        public static ICollection<MessageDTO> GetDTO(this ICollection<Message> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}