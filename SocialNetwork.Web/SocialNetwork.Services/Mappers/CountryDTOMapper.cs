﻿using SocialNetwork.Models;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SocialNetwork.Services.Mappers
{
    public static class CountryDTOMapper
    {
        public static CountryDTO GetDTO(this Country item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new CountryDTO
            {
                Id = item.Id,
                Name = item.Name,
                Users = item.Users
            };
        }

        public static Country GetCountry(this CountryDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Country
            {
                Id = item.Id,
                Name = item.Name,
                Users = item.Users
            };
        }
        public static ICollection<CountryDTO> GetDTO(this ICollection<Country> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
