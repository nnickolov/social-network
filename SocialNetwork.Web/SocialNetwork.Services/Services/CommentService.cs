﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Services
{
    public class CommentService : ICommentService
    {
        private readonly UserManager<User> _userManager;
        private readonly IUserService _userService;
        private readonly SocialNetworkDbContext _context;
        public CommentService(IUserService userService, UserManager<User> userManager, SocialNetworkDbContext context)
        {
            this._context = context;
            this._userManager = userManager;
            this._userService = userService;
        }
        //Async Methods - All Done!
        //OK
        //APIOK
        //ControllerOK
        public async Task<CommentDTO> CreateCommentAsync(int id, int postId, CommentDTO commentDTO)
        {
            var photo = _context.Photos.Where(x => x.PostId == postId).FirstOrDefault();

            if (photo != null)
            {
                var comment = new Comment
                {
                    CreatedOn = DateTime.Now,
                    Content = commentDTO.Content,
                    UserId = id,
                    PhotoId = photo.Id,
                    PostId = postId
                };
                _context.Comments.Add(comment);
                await _context.SaveChangesAsync();

                var historyLog = new HistoryLogDTO
                {
                    Content = $"Just added a new comment at {comment.CreatedOn}.",
                    UserId = id
                };

                _context.HistoryLogs.Add(historyLog.GetHistoryLog());
                await _context.SaveChangesAsync();
                return comment.GetDTO();
            }

            else
            {
                var comment = new Comment
                {
                    CreatedOn = DateTime.Now,
                    Content = commentDTO.Content,
                    UserId = id,
                    PostId = postId
                };
                _context.Comments.Add(comment);
                await _context.SaveChangesAsync();

                var historyLog = new HistoryLogDTO
                {
                    Content = $"Just added a new comment at {comment.CreatedOn}.",
                    UserId = id
                };

                _context.HistoryLogs.Add(historyLog.GetHistoryLog());
                await _context.SaveChangesAsync();
                return comment.GetDTO();
            }
        }
        //OK
        //APIOK
        //ControllerOK
        //TODO ask if it gets the logged user
        public async Task<CommentDTO> DeleteCommentAsync(int commentId, int userId)
        {
            var comment = await this._context.Comments
                .FirstOrDefaultAsync(x => x.Id == commentId);

            _context.Comments.Remove(comment);

            var historyLog = new HistoryLogDTO
            {
                Content = $"Just deleted a comment.",
                UserId = userId
            };

            _context.HistoryLogs.Add(historyLog.GetHistoryLog());

            await _context.SaveChangesAsync();

            return comment.GetDTO();
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<ICollection<CommentDTO>> GetAllCommentsAsync()
        {
            var comments = await (from u in _context.Comments
                                  join c in _context.Users on u.UserId equals c.Id into grp1
                                  join p in _context.Posts on u.PostId equals p.Id into grp2
                                  from g in grp1.DefaultIfEmpty()
                                  from g2 in grp2.DefaultIfEmpty()
                                  where u.IsDeleted == false
                                  select new CommentDTO
                                  {
                                      Id = u.Id,
                                      Content = u.Content,
                                      UserId = u.UserId,
                                      User = u.User,
                                      IsFlagged = u.IsFlagged,
                                      IsLiked = u.IsLiked,
                                      CreatedOn = u.CreatedOn,
                                      PostId = u.PostId,
                                      Post = u.Post,
                                  }).ToListAsync();

            return comments;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<CommentDTO> GetCommentAsync(int id)
        {
            var comment = await (from u in _context.Comments
                                 join c in _context.Users on u.UserId equals c.Id into grp1
                                 join p in _context.Posts on u.PostId equals p.Id into grp2
                                 from g in grp1.DefaultIfEmpty()
                                 from g2 in grp2.DefaultIfEmpty()
                                 where u.Id == id
                                 select new CommentDTO
                                 {
                                     Id = u.Id,
                                     Content = u.Content,
                                     UserId = u.UserId,
                                     User = u.User,
                                     IsFlagged = u.IsFlagged,
                                     IsLiked = u.IsLiked,
                                     CreatedOn = u.CreatedOn,
                                     PostId = u.PostId,
                                     Post = u.Post,
                                 }).FirstOrDefaultAsync();

            return comment;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<CommentDTO> FlagCommentAsync(int id)
        {
            var flag = await this._context.Comments
                 .FirstOrDefaultAsync(x => x.Id == id);

            flag.IsFlagged = true;

            flag.Flags++;

            await _context.SaveChangesAsync();

            return flag.GetDTO();
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<CommentDTO> LikeCommentAsync(int id)
        {
            var comment = await this._context.Comments
                 .FirstOrDefaultAsync(x => x.Id == id);

            comment.IsLiked = true;

            comment.Likes++;

            await _context.SaveChangesAsync();

            return comment.GetDTO();
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<CommentDTO> UpdateCommentAsync(int id, string commentDTO)
        {
            var comment = await this._context.Comments
             .FirstOrDefaultAsync(x => x.Id == id);

            comment.Content = commentDTO;

            await _context.SaveChangesAsync();

            return comment.GetDTO();
        }
    }
}
