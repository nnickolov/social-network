﻿using System.Linq;
using System.Threading.Tasks;
using SocialNetwork.Database;
using System.Collections.Generic;
using SocialNetwork.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using SocialNetwork.Services.Mappers;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Models.ResponseModels;

namespace SocialNetwork.Services.Services
{
    public class CountryService : ICountryService
    {
        private readonly SocialNetworkDbContext _context;
        public CountryService(SocialNetworkDbContext context)
        {
            this._context = context;
        }
        //Async Methods
        //OK
        //APIOK
        //ControllerOK
        public async Task<ResponseModel> CreateCountryAsync(CountryDTO countryDTO)
        {
            var result = new ResponseModel();

            if (_context.Countries.Any(b => b.Name == countryDTO.Name))
            {
                result.Success = false;
                result.Message = "There is a country with such name already.";
            }
            else
            {
                _context.Countries.Add(countryDTO.GetCountry());

                await _context.SaveChangesAsync();

                result.Success = true;
                result.Message = "New country has been created successfully!";
            }

            return result;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<CountryDTO> DeleteCountryAsync(int id)
        {
            var country = await this._context.Countries
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            country.IsDeleted = true;

            await _context.SaveChangesAsync();

            return country.GetDTO();
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<ICollection<CountryDTO>> GetAllCountriesAsync()
        {
            var countries = await (from c in _context.Countries
                                   where c.IsDeleted == false
                                   select new CountryDTO
                                   {
                                       Id = c.Id,
                                       Name = c.Name,
                                       Users = c.Users
                                   }).ToListAsync();

            return countries;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<CountryDTO> GetCountryAsync(int id)
        {
            var country = await (from c in _context.Countries
                                 where c.IsDeleted == false
                                 select new CountryDTO
                                 {
                                     Id = c.Id,
                                     Name = c.Name,
                                     Users = c.Users
                                 }).FirstOrDefaultAsync();

            return country;
        }
        public async Task<CountryDTO> GetCountryAsync(string name)
        {
            var country = await (from c in _context.Countries
                                 where c.Name == name && c.IsDeleted == false
                                 select new CountryDTO
                                 {
                                     Id = c.Id,
                                     Name = c.Name,
                                     Users = c.Users
                                 }).FirstOrDefaultAsync();

            return country;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<CountryDTO> UpdateCountryAsync(int id, string newName)
        {
            var country = await this._context.Countries
                     .FirstOrDefaultAsync(x => x.Id == id);

            country.Name = newName;

            await _context.SaveChangesAsync();

            return country.GetDTO();
        }
    }
}