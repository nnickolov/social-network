﻿using System.Threading.Tasks;
using SocialNetwork.Database;
using SocialNetwork.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using SocialNetwork.Services.Mappers;
using SocialNetwork.Services.Contracts;
using System.Linq;
using System;
using SocialNetwork.Models;

namespace SocialNetwork.Services.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly SocialNetworkDbContext _context;
        public PhotoService(SocialNetworkDbContext context)
        {
            this._context = context;
        }
        //OK
        //APIOK
        public async Task<PhotoDTO> DeletePhotoAsync(int id)
        {
            var photo = await this._context.Photos
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            photo.IsDeleted = true;

            await _context.SaveChangesAsync();

            return photo.GetDTO();
        }
        //OK
        //APIOK
        public async Task<PhotoDTO> UploadPhotoAsync(int loggedUser, int id, PhotoDTO photoDTO)
        {
            var user = _context.Users.Where(x => x.Id == id).FirstOrDefault();

            var post = new PostDTO
            {
                Content = photoDTO.PhotoDescription,
                PostPhoto = photoDTO.Type,
                CreatedOn = DateTime.Now,
                UserId = id,
                LoggedUser = loggedUser
            };

            _context.Posts.Add(post.GetPost());
            await _context.SaveChangesAsync();

            var currPost = _context.Posts.Where(x => x.CreatedOn == post.CreatedOn).FirstOrDefault();

            var photo = new PhotoDTO()
            {
                PhotoAsBytes = photoDTO.PhotoAsBytes,
                PhotoAsNumber = photoDTO.PhotoAsNumber,
                Type = photoDTO.Type,
                PostId = currPost.Id,
                UserId = user.Id,
            };

            if(loggedUser != 0)
            {
                photo.UserId = id;
            }

            _context.Photos.Add(photo.GetPhoto());
            await _context.SaveChangesAsync();

            var currentPhoto = _context.Photos.Where(x => x.PhotoAsNumber == photo.PhotoAsNumber).FirstOrDefault();

            currPost.PhotoId = currentPhoto.Id;

            await _context.SaveChangesAsync();

            var historyLog = new HistoryLogDTO
            {
                Content = $"{user.FirstName} added a new photo at {currPost.CreatedOn}.",
                UserId = user.Id
            };

            _context.HistoryLogs.Add(historyLog.GetHistoryLog());
            await _context.SaveChangesAsync();

            return photoDTO;
        }
      
        public async Task<PhotoDTO> UploadCoverPhotoAsync(int id, PhotoDTO photoDTOCover)
        {
            var user = _context.Users.Where(x => x.Id == id).FirstOrDefault();

            var post = new PostDTO
            {
                Content = "Cover Photo",
                PostPhoto = photoDTOCover.Type,
                CreatedOn = DateTime.Now,
                UserId = user.Id
            };

         

            _context.Posts.Add(post.GetPost());

            await _context.SaveChangesAsync();

            var currPost = _context.Posts.Where(x => x.PostPhoto == photoDTOCover.Type).FirstOrDefault();

            var photoCover = new PhotoDTO()
            {
                PhotoAsBytes = photoDTOCover.PhotoAsBytes,
                PhotoAsNumber = photoDTOCover.PhotoAsNumber,
                Type = photoDTOCover.Type,
                PostId = currPost.Id,
                UserId = user.Id,
            };

            _context.Photos.Add(photoCover.GetPhoto());
            await _context.SaveChangesAsync();

            var currentPhoto = _context.Photos.Where(x => x.PhotoAsNumber == photoCover.PhotoAsNumber).FirstOrDefault();

            currPost.PhotoId = currentPhoto.Id;

            await _context.SaveChangesAsync();

            var historyLog = new HistoryLogDTO
            {
                Content = $"{user.FirstName} added a new cover photo at {currPost.CreatedOn}.",
                UserId = user.Id
            };

            _context.HistoryLogs.Add(historyLog.GetHistoryLog());
            await _context.SaveChangesAsync();

            return photoDTOCover;
        }
    }
}
