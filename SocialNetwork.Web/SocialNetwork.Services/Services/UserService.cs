﻿using Microsoft.EntityFrameworkCore;
using SocialNetwork.Database;
using SocialNetwork.Models.ResponseModels;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services
{
    public class UserService : IUserService
    {
        private readonly SocialNetworkDbContext _context;
        public UserService(SocialNetworkDbContext context)
        {
            _context = context;
        }

        //OK
        //APIOK
        //ControllerOK5
        public async Task<ResponseModel> CreateUserAsync(UserDTO userDTO)
        {
            var result = new ResponseModel();

            if (_context.Users.Any(b => b.Email == userDTO.Email))
            {
                result.Success = false;
                result.Message = "There is user with such email already.";
            }
            else
            {
                if (_context.Countries.Any(x => x.Name == userDTO.Country.Name))
                {
                    var country = _context.Countries.Where(x => x.Name == userDTO.Country.Name).FirstOrDefault();

                    userDTO.CountryId = country.Id;
                    userDTO.UserName = userDTO.Email;
                    userDTO.NormalizedEmail = userDTO.Email.ToUpper();
                    userDTO.NormalizedUserName = userDTO.Email.ToUpper();
                    _context.Users.Add(userDTO.GetUser());
                    await _context.SaveChangesAsync();
                }

                else
                {
                    var country = new CountryDTO();
                    country.Name = userDTO.Country.Name;
                    _context.Countries.Add(country.GetCountry());
                    await _context.SaveChangesAsync();

                    var newCountry = _context.Countries.Where(x => x.Name == country.Name).FirstOrDefault();
                    userDTO.CountryId = newCountry.Id;
                    userDTO.UserName = userDTO.Email;
                    userDTO.NormalizedEmail = userDTO.Email.ToUpper();
                    userDTO.NormalizedUserName = userDTO.Email.ToUpper();
                    _context.Users.Add(userDTO.GetUser());
                    await _context.SaveChangesAsync();
                }
                result.Success = true;
                result.Message = "You have been registered!";
            }
            return result;
        }

        public async Task<ICollection<PostDTO>> GetNewsFeedAsync(int id)
        {
            var user = await GetUserAsync(id);

            var friendList = user.Friends;

            var newsFeed = new List<PostDTO>();

            foreach (var u in user.Posts)
            {
                newsFeed.Add(u.GetDTO());
            }

            foreach (var friend in friendList)
            {
                foreach (var post in friend.Posts)
                {
                    newsFeed.Add(post.GetDTO());
                }
            }

            newsFeed.OrderByDescending(x => x.CreatedOn);

            return newsFeed;
            //var userFriends = await (from u in _context.Users
            //                         join fr in _context.UserFriends on u.Id equals fr.RequesterId into grp2
            //                         from g1 in grp2.DefaultIfEmpty()
            //                         join fa in _context.UserFriends on u.Id equals fa.AdresseeId into grp3
            //                         from g2 in grp3.DefaultIfEmpty()
            //                         where u.Id == id && u.IsDeleted == false
            //                         select new
            //                         {
            //                             allFriendsIds = grp2.Select(x => x.AdresseeId).Union(grp3.Select(x => x.RequesterId)).ToList(),
            //                         }).FirstOrDefaultAsync();

            //var user = _context.Users.Where(x => x.Id == id).FirstOrDefault();

            //var newsFeed = await _context.Posts.Where(x => userFriends.allFriendsIds
            //    .Contains(x.UserId))
            //    .Include(p => p.Comments)
            //    .Select(x => new PostDTO
            //    {
            //        Content = x.Content,
            //        //TODO
            //    }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            //foreach (var u in newsFeed)
            //{
            //    var post = u.GetPost();
            //    user.NewsFeed.Add(post);
            //}

            //return newsFeed;
        }

        //OK
        //APIOK  
        //ControllerOK
        public async Task<UserDTO> DeleteUserAsync(int id)
        {
            var user = await this._context.Users
                .FirstOrDefaultAsync(x => x.Id == id);

            user.IsDeleted = true;
            user.EmailConfirmed = false;

            await _context.SaveChangesAsync();

            return user.GetDTO();
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<ICollection<UserDTO>> GetAllUsersAsync()
        {
            var users = await (from x in _context.Users
                               join c in _context.Countries on x.CountryId equals c.Id into grp1
                               from g in grp1.DefaultIfEmpty()
                               where x.IsDeleted == false
                               select new UserDTO
                               {
                                   Id = x.Id,
                                   UserName = x.UserName,
                                   ProfilePhoto = x.ProfilePhoto,
                                   Photos = x.Photos,
                                   Friends = x.Friends,
                                   Education = x.Education,
                                   CoverPhoto = x.CoverPhoto,
                                   Posts = x.Posts,
                                   Email = x.Email,
                                   DateOfBirth = x.DateOfBirth,
                                   MonthOfBirth = x.MonthOfBirth,
                                   YearOfBirth = x.YearOfBirth,
                                   FirstName = x.FirstName,
                                   Biography = x.Biography,
                                   LastName = x.LastName,
                                   CountryId = x.Id,
                                   Country = x.Country,
                                   City = x.City,
                                   HistoryLog = x.HistoryLog,
                                   Gender = x.Gender,
                                   JobTitle = x.JobTitle,
                                   RequestersUserFriends = x.RequestersUserFriends,
                                   RelationshipStatus = x.RelationshipStatus,
                                   // Comments = x.Comments.Select(o => new Comment { Id = o.Id, Content = o.Content, User = o.User }).ToList(),
                               }).ToListAsync();

            return users;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<UserDTO> GetUserAsync(int id)
        {
            var user = await _context.Users.Where(u => u.Id == id)
                .Include(u => u.RequestersUserFriends)
                   .ThenInclude(d => d.Requester)
                      .ThenInclude(r => r.Posts)
                         .ThenInclude(p => p.Comments)
                .Include(u => u.RequestersUserFriends)
                   .ThenInclude(d => d.Adressee)
                      .ThenInclude(a => a.Posts)
                         .ThenInclude(p => p.Comments)
                .Include(u => u.AdresseesUserFriends)
                    .ThenInclude(d => d.Adressee)
                      .ThenInclude(a => a.Posts)
                         .ThenInclude(p => p.Comments)
                .Include(u => u.AdresseesUserFriends)
                    .ThenInclude(d => d.Requester)
                      .ThenInclude(r => r.Posts)
                         .ThenInclude(p => p.Comments)
                .Include(u => u.Friends)
                .Include(u => u.Messages)
                .Include(u => u.Country)
                .Include(u => u.HistoryLog)
                .Include(u => u.Photos)
                    .ThenInclude(p => p.Post)
                      .ThenInclude(u => u.User)
                .Include(u => u.Posts)
                    .ThenInclude(u => u.Comments)
                      .ThenInclude(u => u.User)
                .Select(x => new UserDTO
                {
                    Id = x.Id,
                    UserName = x.UserName,
                    ProfilePhoto = x.ProfilePhoto,
                    Photos = x.Photos,
                    CoverPhoto = x.CoverPhoto,
                    Education = x.Education,
                    Posts = x.Posts,
                    Friends = x.Friends,
                    Email = x.Email,
                    CurrentLoggedUserId = x.CurrentLoggedUserId,
                    RequestersUserFriends = x.RequestersUserFriends,
                    AdresseesUserFriends = x.AdresseesUserFriends,
                    CurrentLoggedUser = x.CurrentLoggedUser,
                    DateOfBirth = x.DateOfBirth,
                    MonthOfBirth = x.MonthOfBirth,
                    LoggedUserProfilePicture = x.LoggedUserProfilePicture,
                    YearOfBirth = x.YearOfBirth,
                    FirstName = x.FirstName,
                    Biography = x.Biography,
                    NewsFeed = x.NewsFeed,
                    LastName = x.LastName,
                    CountryId = x.Id,
                    Messages = x.Messages,
                    Country = x.Country,
                    City = x.City,
                    Visibility = x.Visibility,
                    HistoryLog = x.HistoryLog,
                    Gender = x.Gender,
                    JobTitle = x.JobTitle,
                    RelationshipStatus = x.RelationshipStatus,
                    Comments = x.Comments
                }).SingleOrDefaultAsync();

            try
            {
                user.Friends = (user.AdresseesUserFriends.Select(x => x.Adressee).ToList()).ToList().Union(user.RequestersUserFriends.Select(u => u.Requester).ToList()).ToList();
            }
            catch
            {
                throw new System.Exception();
            }
            // user.Posts = (user.Photos.Select(x => x.Post).ToList()).ToList().Union(user.Comments.Select(cont => cont.Post).ToList()).ToList();
            return user;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<UserDTO> GetUserAsync(string email)
        {
            var user = await (from u in _context.Users
                              join c in _context.Countries on u.CountryId equals c.Id into grp1
                              from g in grp1.DefaultIfEmpty()
                              where u.Email == email && u.IsDeleted == false
                              select new UserDTO
                              {
                                  Id = u.Id,
                                  CountryId = u.CountryId,
                                  UserName = u.UserName,
                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  Friends = u.Friends,
                                  Comments = u.Comments,
                                  DateOfBirth = u.DateOfBirth,
                                  MonthOfBirth = u.MonthOfBirth,
                                  YearOfBirth = u.YearOfBirth,
                                  Gender = u.Gender,
                                  City = u.City,
                                  Email = u.Email,
                                  NewsFeed = u.NewsFeed
                              }).FirstOrDefaultAsync();
            return user;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<UserDTO> UpdateUserInformationAfterLoginAsync(int id, UserDTO userDTO)
        {
            var user = await this._context.Users
            .Where(x => x.Id == id).FirstOrDefaultAsync();

            user.JobTitle = userDTO.JobTitle;
            user.City = userDTO.City;
            user.ProfilePhoto = userDTO.ProfilePhoto;
            user.CoverPhoto = userDTO.CoverPhoto;
            user.Biography = userDTO.Biography;
            user.Education = userDTO.Education;
            user.Visibility = userDTO.Visibility;

            if (_context.Countries.Any(x => x.Name == userDTO.Country.Name))
            {
                var country = _context.Countries.Where(x => x.Name == userDTO.Country.Name).FirstOrDefault();

                user.CountryId = country.Id;
            }

            else
            {
                var country = new CountryDTO();
                country.Name = userDTO.Country.Name;
                _context.Countries.Add(country.GetCountry());
                await _context.SaveChangesAsync();

                var newCountry = _context.Countries.Where(x => x.Name == country.Name).FirstOrDefault();
                user.CountryId = newCountry.Id;
            }

            await _context.SaveChangesAsync();

            return user.GetDTO();
        }

        public async Task<UserDTO> UpdateExistingUserInformationAsync(int id, UserDTO userDTO)
        {
            var user = await this._context.Users
            .Where(x => x.Id == id).FirstOrDefaultAsync();

            //FirstName - OK
            if (userDTO.FirstName == null)
            {
                user.FirstName = user.FirstName;
            }
            else
            {
                user.FirstName = userDTO.FirstName;
            }
            //LastName - OK
            if (userDTO.LastName == null)
            {
                user.LastName = user.LastName;
            }
            else
            {
                user.LastName = userDTO.LastName;
            }
            if (userDTO.Visibility == 0)
            {
                user.Visibility = userDTO.Visibility;
            }
            else
            {
                user.Visibility = userDTO.Visibility;
            }
            //Email - OK
            if (userDTO.Email == null)
            {
                user.Email = user.Email;
                user.NormalizedEmail = user.Email.ToUpper();
                user.NormalizedUserName = user.Email.ToUpper();
                user.UserName = user.Email;
            }
            else
            {
                user.Email = userDTO.Email;
                user.NormalizedEmail = userDTO.Email.ToUpper();
                user.NormalizedUserName = userDTO.Email.ToUpper();
                user.UserName = userDTO.Email;
            }
            //DateOfBirth - OK
            if (userDTO.DateOfBirth == null || userDTO.DateOfBirth == "Day")
            {
                user.DateOfBirth = user.DateOfBirth;
            }
            else
            {
                user.DateOfBirth = userDTO.DateOfBirth;
            }
            //MonthOfBirht - OK
            if (userDTO.MonthOfBirth == null || userDTO.MonthOfBirth == "Month")
            {
                user.MonthOfBirth = user.MonthOfBirth;
            }
            else
            {
                user.MonthOfBirth = userDTO.MonthOfBirth;
            }
            //YearOfBirth - OK
            if (userDTO.YearOfBirth == null || userDTO.YearOfBirth == "Year")
            {
                user.YearOfBirth = user.YearOfBirth;
            }
            else
            {
                user.YearOfBirth = userDTO.YearOfBirth;
            }
            //Country - OK
            if (userDTO.Country == null)
            {
                user.Country = user.Country;
            }
            else
            {
                user.Country = userDTO.Country;
            }
            //JobTitle
            if (userDTO.JobTitle == null)
            {
                user.JobTitle = user.JobTitle;
            }
            else
            {
                user.JobTitle = userDTO.JobTitle;
            }
            //City
            if (userDTO.City == null)
            {
                user.City = user.City;
            }
            else
            {
                user.City = userDTO.City;
            }
            //Biography
            if (userDTO.Biography == null)
            {
                user.Biography = user.Biography;
            }
            else
            {
                user.Biography = userDTO.Biography;
            }
            //Education
            if (userDTO.Education == null)
            {
                user.Education = user.Education;
            }
            else
            {
                user.Education = userDTO.Education;
            }
            //Profile photo
            if (userDTO.ProfilePhoto == null)
            {
                user.ProfilePhoto = user.ProfilePhoto;
            }
            else
            {
                user.ProfilePhoto = userDTO.ProfilePhoto;
            }
            //Cover Photo
            if (userDTO.CoverPhoto == null)
            {
                user.CoverPhoto = user.CoverPhoto;
            }
            else
            {
                user.CoverPhoto = userDTO.CoverPhoto;
            }

            user.Gender = userDTO.Gender;

            if (_context.Countries.Any(x => x.Name == userDTO.Country.Name))
            {
                var country = _context.Countries.Where(x => x.Name == userDTO.Country.Name).FirstOrDefault();

                user.CountryId = country.Id;
            }

            else
            {
                var country = new CountryDTO();
                country.Name = userDTO.Country.Name;
                _context.Countries.Add(country.GetCountry());
                await _context.SaveChangesAsync();

                var newCountry = _context.Countries.Where(x => x.Name == country.Name).FirstOrDefault();
                user.CountryId = newCountry.Id;
            }

            await _context.SaveChangesAsync();

            return user.GetDTO();
        }
    }
}
