﻿using SocialNetwork.Database;
using SocialNetwork.Models.ResponseModels;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Mappers
{
    public class UserFriendService : IUserFriendService
    {
        private readonly SocialNetworkDbContext _context;
        private readonly IUserService _userService;

        public UserFriendService(SocialNetworkDbContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }


        public async Task<ResponseModel> AcceptFriendRequest(int Id)
        {
            //var accepter = await _userService.GetUserAsync(accepterId);
            //var requester = await _userService.GetUserAsync(requesterId);

            //var accepterUser = accepter.GetUser();
            //var requesterUser = requester.GetUser();
            //accepterUser.Friends.Add(requesterUser);
            //requesterUser.Friends.Add(accepterUser);


            var toAccept = _context.UserFriends.Where(x => x.Id == Id).FirstOrDefault();

            toAccept.IsAccepted = true;
            toAccept.IsRemoved = false;

            await _context.SaveChangesAsync();

            var response = new ResponseModel();

            response.Message = "Request has been accepted successfully!";
            response.Success = true;

            return response;
        }

        public async Task<ResponseModel> SendFriendRequest(int requesterId, int adresseeId)
        {
            var requester = _context.Users.Where(x => x.Id == requesterId).FirstOrDefault();
            var adressee = _context.Users.Where(x => x.Id == adresseeId).FirstOrDefault();

            requester.GetDTO();
            adressee.GetDTO();

            var friendRequest = new UserFriendDTO()
            {
                Requester = requester,
                RequesterId = requesterId,
                Adressee = adressee,
                AdresseeId = adresseeId,
            };

            var response = new ResponseModel();

            if ((!_context.UserFriends.Any(x => x.RequesterId == requesterId && x.AdresseeId == adresseeId && x.IsAccepted == false && x.IsRemoved == false)) || (!_context.UserFriends.Any(x => x.RequesterId == adresseeId && x.AdresseeId == requesterId && x.IsAccepted == false && x.IsRemoved == false)))
            {
                _context.UserFriends.Add(friendRequest.GetUserFriend());
            }
            //else if (!_context.UserFriends.Any(x => x.RequesterId == adresseeId && x.AdresseeId == requesterId && x.IsAccepted == false && x.IsRemoved == false))
            //{
            //    _context.UserFriends.Add(friendRequest.GetUserFriend());
            //}
            else if (_context.UserFriends.Any(x => x.RequesterId == requesterId && x.AdresseeId == adresseeId && x.IsAccepted == false && x.IsRemoved == true))
            {
                var request = _context.UserFriends.Where(x => x.RequesterId == requesterId && x.AdresseeId == adresseeId && x.IsAccepted == false && x.IsRemoved == true).FirstOrDefault();

                request.IsRemoved = false;
            }

            _context.SaveChanges();

            return response;
        }

        public async Task<ResponseModel> RemoveFriend(int Id)
        {
            //var accepterUser = accepter.GetUser();
            //var requesterUser = requester.GetUser();
            //accepterUser.Friends.Add(requesterUser);
            //requesterUser.Friends.Add(accepterUser);

            var isRemoved = _context.UserFriends.Where(x => x.Id == Id).FirstOrDefault();

            isRemoved.IsRemoved = true;
            isRemoved.IsAccepted = false;

            await _context.SaveChangesAsync();

            var response = new ResponseModel();

            response.Message = "Request has been accepted successfully!";
            response.Success = true;

            return response;
        }

        public async Task<ResponseModel> DeclineFriendRequest(int id)
        {
            var isRemoved = _context.UserFriends.Where(x => x.Id == id).FirstOrDefault();

            isRemoved.IsRemoved = true;
            isRemoved.IsAccepted = false;

            await _context.SaveChangesAsync();

            var response = new ResponseModel();

            response.Message = "Request has been accepted successfully!";
            response.Success = true;

            return response;
        }
    }
}
