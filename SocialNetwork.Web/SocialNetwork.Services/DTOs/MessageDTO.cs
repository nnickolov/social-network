﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SocialNetwork.Services.DTOs
{
    public class MessageDTO
    {
        public int Id { get; set; }
        public int? SenderId { get; set; }
        public virtual User Sender { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime SendOn { get; set; } = DateTime.Now;
        public int ReceiverId { get; set; }
        public virtual User Receiver { get; set; }
        public bool isSeen { get; set; }
        public bool IsDeleted { get; set; }
    }
}