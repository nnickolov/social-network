﻿using Microsoft.AspNetCore.Http;
using SocialNetwork.Models;
using SocialNetwork.Models.Enums;
using System.Collections.Generic;

namespace SocialNetwork.Services.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string NormalizedEmail { get; set; }
        public int CurrentLoggedUser { get; set; }
        public string NormalizedUserName { get; set; }
        public int CurrentLoggedUserId { get; set; }
        public string PasswordHash { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public string DateOfBirth { get; set; }
        public string MonthOfBirth { get; set; }
        public string YearOfBirth { get; set; }
        public string ProfilePhoto { get; set; }
        public string Gender { get; set; }
        public string Education { get; set; }
        public bool IsDeleted { get; set; }
        public string JobTitle { get; set; }
        public string City { get; set; }
        public string LoggedUserProfilePicture { get; set; }
        public string Biography { get; set; }
        public string CoverPhoto { get; set; }
        public bool IsAdmin { get; set; }
        public string NewCommentContent { get; set; }
        public Visibility Visibility { get; set; }
        public ICollection<Photo> Photos { get; set; }
        public ICollection<HistoryLog> HistoryLog { get; set; } = new List<HistoryLog>();
        public RelationshipStatus RelationshipStatus { get; set; }
        public virtual ICollection<UserFriend> RequestersUserFriends { get; set; }
        public virtual ICollection<UserFriend> AdresseesUserFriends { get; set; }
        public virtual ICollection<User> Friends { get; set; }
        public ICollection<int> Friends1 { get; set; }
        public ICollection<int> Friends2 { get; set; }
        public ICollection<Post> NewsFeed { get; set; }
        public ICollection<Post> Posts { get; set; }
        public ICollection<Message> Messages { get; set; }
        public ICollection<Comment> Comments { get; set; }

    }
}
