﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.DTOs
{
    public class UserFriendDTO
    {
        public int Id { get; set; }
        public int RequesterId { get; set; }
        public User Requester { get; set; }
        public int AdresseeId { get; set; }
        public User Adressee { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsRemoved { get; set; }
    }
}
