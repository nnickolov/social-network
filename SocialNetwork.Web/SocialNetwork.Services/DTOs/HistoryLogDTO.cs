﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.DTOs
{
    public class HistoryLogDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
    }
}
