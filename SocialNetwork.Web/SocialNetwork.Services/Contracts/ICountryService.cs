﻿using SocialNetwork.Models.ResponseModels;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Contracts
{
    public interface ICountryService
    {
        Task<CountryDTO> GetCountryAsync(int id);
        Task<ResponseModel> CreateCountryAsync(CountryDTO countryDTO);
        Task<CountryDTO> UpdateCountryAsync(int id, string newName);
        Task<CountryDTO> GetCountryAsync(string name);
        Task<CountryDTO> DeleteCountryAsync(int id);
        Task<ICollection<CountryDTO>> GetAllCountriesAsync();
    }
}
