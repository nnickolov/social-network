﻿using SocialNetwork.Models.ResponseModels;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Contracts
{
    public interface ICommentService
    {
        Task<CommentDTO> CreateCommentAsync(int id, int postId, CommentDTO commentDTO);
        Task<CommentDTO> DeleteCommentAsync(int id, int userId);
        Task<CommentDTO> GetCommentAsync(int id);
        Task<ICollection<CommentDTO>> GetAllCommentsAsync();
        Task<CommentDTO> LikeCommentAsync(int id);
        Task<CommentDTO> FlagCommentAsync(int id);
        Task<CommentDTO> UpdateCommentAsync(int id, string commentDTO);
    }
}
