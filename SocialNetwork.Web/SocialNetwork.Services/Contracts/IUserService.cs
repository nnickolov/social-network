﻿using SocialNetwork.Models;
using SocialNetwork.Models.ResponseModels;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Contracts
{
    public interface IUserService
    {
        Task<UserDTO> DeleteUserAsync(int id);
        Task<ResponseModel> CreateUserAsync(UserDTO userDTO);
        Task<UserDTO> GetUserAsync(int id);
        Task<UserDTO> GetUserAsync(string email);
        Task<ICollection<UserDTO>> GetAllUsersAsync();
        Task<ICollection<PostDTO>> GetNewsFeedAsync(int id);
        Task<UserDTO> UpdateUserInformationAfterLoginAsync(int id, UserDTO userDTO);
        Task<UserDTO> UpdateExistingUserInformationAsync(int id, UserDTO userDTO);
    }
}
