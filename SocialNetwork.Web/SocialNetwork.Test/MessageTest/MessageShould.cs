﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using SocialNetwork.Services.Services;
using System;
using System.Threading.Tasks;

namespace SocialNetwork.Test
{
    [TestClass]
    public class MessageShould
    {
        [TestMethod]
        public async Task Delete_Message_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Delete_Message_Should));

            var message = new Message
            {
                Id = 1,
                SendOn = DateTime.Now,
                ReceiverId = 1
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Messages.Add(message);
                await arrangeContext.SaveChangesAsync();
                var sut = new MessageService(arrangeContext);
                arrangeContext.Messages.Remove(message);
                Assert.IsTrue(message.IsDeleted == false);
            }
        }
    }
}
