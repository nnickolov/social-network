﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using SocialNetwork.Services.Services;
using System.Threading.Tasks;

namespace SocialNetwork.Test
{
    [TestClass]
    public class PhotoShould
    {
        [TestMethod]
        public async Task Create_Photo_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Create_Photo_Should));

            var photo = new Photo
            {
                Id = 1,
                PostId = 1,
                UserId = 1
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Photos.Add(photo);
                await arrangeContext.SaveChangesAsync();
                var sut = new PhotoService(arrangeContext);
                var photoDTO = new PhotoDTO();

                photoDTO = photo.GetDTO();
                var result = sut.UploadPhotoAsync(1, photoDTO);
                Assert.AreEqual(photo.Id, result.Id);
            }
        }
        [TestMethod]
        public async Task Delete_Photo_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Delete_Photo_Should));

            var photo = new Photo
            {
                Id = 1,
                PostId = 1,
                UserId = 1
            };


            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Photos.Add(photo);
                await arrangeContext.SaveChangesAsync();
                var sut = new PhotoService(arrangeContext);
                await sut.DeletePhotoAsync(1);
                Assert.IsTrue(photo.IsDeleted == true);
            }
        }
    }
}