﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using SocialNetwork.Services.Services;
using System.Threading.Tasks;

namespace SocialNetwork.Test
{
    [TestClass]
    public class CounttyShould
    {
        [TestMethod]
        public async Task Create_Country_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Create_Country_Should));

            var country = new Country
            {
                Id = 1,
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                await arrangeContext.SaveChangesAsync();
                var sut = new CountryService(arrangeContext);
                var result = await sut.GetCountryAsync(1);
                Assert.AreEqual(country.Id, result.Id);
            }
        }
        [TestMethod]
        public async Task Delete_Country_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Delete_Country_Should));

            var country = new Country
            {
                Id = 1,
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                await arrangeContext.SaveChangesAsync();
                arrangeContext.Countries.Remove(country);
                var sut = new CountryService(arrangeContext);
                var result = await sut.GetCountryAsync(1);
                Assert.IsTrue(country.IsDeleted = true);
            }
        }
       
        [TestMethod]
        public async Task Get_Country_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Get_Country_Should));

            var country = new Country
            {
                Id = 1,
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                await arrangeContext.SaveChangesAsync();
                var sut = new CountryService(arrangeContext);
                var result = await sut.GetCountryAsync(1);
                Assert.AreEqual(country.Id, result.Id);
            }
        }
    }
}
