using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialNetwork.Database;

namespace SocialNetwork.Test
{
    public class Utils
    {
        public static DbContextOptions<SocialNetworkDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<SocialNetworkDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}